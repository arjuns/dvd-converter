﻿using System;
using System.IO;
using Caliburn.Micro;
using ConverterCore.Converters;
using ConverterCore.Encoders;
using ConverterCore.FFMpeg;
using VideoConverter.Views.FileInfo;

namespace VideoConverter
{
    public interface IVideoEncodingService
    {
        MediaInformation GetVideoInfo(string file);
        void Run();
        event EventHandler<EncodeProgressEventArgs> Progress;
        event EventHandler<EncodeFinishedEventArgs> SingleDone;
        string GenerateThumbImage(string file, string thumbPath, MediaInformation mediaInformation);



    }

    public class SingleVideoEncodingService : IHandle<InputVideoViewModel>
    {
        public string OutputFolder { get; set; }
        public IMediaConverter Converter { get; set; }

        private static void RemoveIfFileExists(string outputFile)
        {
            if (File.Exists(outputFile))
                File.Delete(outputFile);
        }

        private static void EnsureDirectoryExists(string outputFolder)
        {

            if (!Directory.Exists(outputFolder))
                Directory.CreateDirectory(outputFolder);

        }

        private string GetSaveFileName(MediaInformation file, string outputFolder)
        {

            var extension = this.Converter.Extension;
            string fileNameOnly = Path.GetFileNameWithoutExtension(file.OriginalFilePath) + extension;
            return Path.Combine(outputFolder, fileNameOnly);
        }

        public event EventHandler<EncodeProgressEventArgs> Progress;
        public event EventHandler<EncodeFinishedEventArgs> Done;

        private EncoderBase actualEncoder;

        public SingleVideoEncodingService()
        {
            IoC.Get<IEventAggregator>().Subscribe(this);
        }

        public void BeginEncodeVideo(MediaInformation media)
        {
            var outputFile = GetSaveFileName(media, OutputFolder);
            EnsureDirectoryExists(OutputFolder);
            RemoveIfFileExists(outputFile);
            actualEncoder = Converter.CreateEncoder(media, outputFile);
            HookUpHandlers();
            actualEncoder.BeginEncode();

        }

        private void HookUpHandlers()
        {
            actualEncoder.Progress += (a, b) =>
                {
                    if (this.Progress != null)
                        Progress(a, b);
                };
            actualEncoder.VideoFinished += (a, b) =>
                {
                    if (this.Done != null)
                        Done(a, b);
                };
            actualEncoder.Progress += (a, b) =>
                {
                    if (this.Progress != null)
                        Progress(a, b);
                };
        }

        public void Handle(InputVideoViewModel message)
        {
            actualEncoder.Cancel();
        }
    }
}