namespace VideoConverter.Skin
{
    public enum ApplicationSkin
    {
        Red,
        Blue,
        Green,
        Orange,
        Purple
    }
}