﻿using System;
using System.Collections.Generic;
using System.Windows;
using Caliburn.Micro;

namespace VideoConverter
{
    public class MyWindowManager : WindowManager
    {


        private static readonly object Sync = new object();

        private static readonly Dictionary<Type, Window> WindowCache;
        static MyWindowManager()
        {
            WindowCache = new Dictionary<Type, Window>();
        }
        public override bool? ShowDialog(object rootModel, object context = null,
                                         IDictionary<string, object> settings = null)
        {
            lock (Sync)
            {


                Type modelType = rootModel.GetType();
                if (!WindowCache.ContainsKey(modelType))
                {
                    Window window = CreateWindow(rootModel, true, context, settings);
                    WindowCache.Add(modelType, window);
                    window.Closed += (a, b) =>
                        {
                            lock (Sync)
                            {
                                WindowCache.Remove(modelType);
                            }

                        };
                    return window.ShowDialog();

                }

                WindowCache[modelType].Activate();
                return null;
            }
        }

    }
}