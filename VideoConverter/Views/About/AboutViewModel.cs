﻿using Caliburn.Micro;
using VideoConverter.Features;

namespace VideoConverter.Views.About
{
    public class AboutViewModel : Screen
    {
       
        public string ApplicationName
        {
            get { return FeatureSelector.Current.ApplicationName; }
            set
            {

            }
        }
        

        public string Version
        {
            get
            {
                return FeatureSelector.Current.Version;
            }
        }

        public override string DisplayName
        {
            get { return "About"; }
            set{}
        }
    }
}
