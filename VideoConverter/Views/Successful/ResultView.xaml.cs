﻿using System.Diagnostics;
using System.Windows;

namespace VideoConverter.Views.Successful
{
    /// <summary>
    /// Interaction logic for ConversionDoneView.xaml
    /// </summary>
    public partial class ResultView : Window
    {
        public ResultView()
        {
            InitializeComponent();
        }

        private void AshampooClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start("https://www.ashampoo.com/en/usd/pin/0710/Burning_Software/Ashampoo-Burning-Studio-6-FREE");
            }
            catch{}
        }
    }
}
