﻿using System;
using System.Diagnostics;
using Caliburn.Micro;

namespace VideoConverter.Views.Successful
{
    public class ResultViewModel:Screen
    {
        public override string DisplayName
        {
            get { return "Finished Conversion"; }
            set
            {
                base.DisplayName = value;
            }
        }
        private string outputFolder;
        public string OutputFolder
        {
            get { return outputFolder; }
            set
            {
                if (value == outputFolder) return;
                outputFolder = value;
                NotifyOfPropertyChange(() => OutputFolder);
            }
        }
        public void BrowseFolder()
        {
            try
            {
                Process.Start(this.OutputFolder);
            }
            catch (Exception x)
            {
                
            }
        }
        public void CloseWindow()
        {
            TryClose();
        }
    }
}
