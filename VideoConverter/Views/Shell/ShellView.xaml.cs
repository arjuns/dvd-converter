﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using Caliburn.Micro;
using Hardcodet.Wpf.TaskbarNotification;
using VideoConverter.Configurations;
using VideoConverter.Features;
using VideoConverter.Skin;
using VideoConverter.Views.About;
using Button = System.Windows.Controls.Button;
using MessageBox = System.Windows.MessageBox;

namespace VideoConverter.Views.Shell
{
    /// <summary>
    /// Interaction logic for Shell.xaml
    /// </summary>
    public partial class ShellView : TrayWindow
    {
        public ShellView()
        {


            InitializeComponent();
            this.Loaded += WindowLoaded;
            this.NotifyIconHost = this.TrayIcon;

        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            var skin = (ApplicationSkin)ConfigReader.GetSkin();
            ApplicationSkinManager.ChangeSkin(this, skin);

        }



        private void ChangeSkin(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button != null)
            {
                ApplicationSkinManager.ChangeSkin(this, (ApplicationSkin)button.Tag);
            }
        }

        private void ExitMenuClicked(object sender, RoutedEventArgs e)
        {
            Process[] processesByName = Process.GetProcessesByName("ffmpeg");
            if (processesByName.Length > 0)
            {
                MessageBoxResult boxResult = MessageBox.Show("It looks that media conversion(s) are running.\r Are you sure you want to close?", FeatureSelector.Current.ApplicationName, MessageBoxButton.YesNo);
                if (boxResult == MessageBoxResult.Yes)
                {
                    processesByName.ToList().ForEach(x => x.Kill());
                    ExitApplication();
                }
            }
            else
            {
                ExitApplication();
            }

        }

        private void ExitApplication()
        {
            this.NotifyIconHost.Dispose();
            Environment.Exit(0);
        }

        private void AboutClicked(object sender, RoutedEventArgs e)
        {
            var manager = IoC.Get<IWindowManager>();
            IDictionary<string, object> settings = new Dictionary<string, object>();
            settings["WindowStartupLocation"] = WindowStartupLocation.CenterOwner;
            manager.ShowDialog(new AboutViewModel(),settings:settings);
        }

        private void OpenClicked(object sender, RoutedEventArgs e)
        {

            this.TrayActivated(sender,e);
        }
    }


    public class TrayWindow : Window
    {
        private WindowState winState;
        private bool goneToTray;
        private TaskbarIcon notifyIconHost;

        protected void TrayActivated(object sender, RoutedEventArgs e)
        {
            Show();
            Topmost = true;
            WindowState = winState;
            Topmost = false;

        }
        public TaskbarIcon NotifyIconHost
        {
            get { return notifyIconHost; }
            set
            {
                notifyIconHost = value;
                this.notifyIconHost.TrayMouseDoubleClick += (a, b) =>
                    {
                        this.notifyIconHost.ToolTipText = FeatureSelector.Current.ApplicationName;
                        TrayActivated(null, null);
                    };

            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            ShowInfo();
            e.Cancel = true;
            Hide();
            base.OnClosing(e);
        }


        protected override void OnStateChanged(EventArgs e)
        {

            if (WindowState == WindowState.Minimized)
            {
                ShowInfo();
                this.ShowInTaskbar = false;
                Hide();
            }
            else
            {
                winState = WindowState;
                this.ShowInTaskbar = true;
            }
            base.OnStateChanged(e);
        }

        private void ShowInfo()
        {
            if (!goneToTray)
            {
                goneToTray = true;
                if (this.NotifyIconHost != null)
                    this.NotifyIconHost.ShowBalloonTip(FeatureSelector.Current.ApplicationName, "Hi, I am here", BalloonIcon.Info);
            }
        }
    }
}
