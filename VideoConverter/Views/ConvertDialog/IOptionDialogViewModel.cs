﻿using ConverterCore.Converters;

namespace VideoConverter.Views.ConvertDialog
{
    public interface IOptionDialogViewModel
    {
        IMediaConverter SelectedConverter { get; set; }
        string OutputFolder { get;  }
    }
}