﻿namespace VideoConverter.Views.ConvertDialog
{
    public enum VideoFormat
    {
        Mkv,
        Avi,
        Mp4,
    }
}