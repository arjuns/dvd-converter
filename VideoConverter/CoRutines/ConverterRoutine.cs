using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Caliburn.Micro;
using ConverterCore.Converters;
using ConverterCore.Encoders;
using ConverterCore.FFMpeg;
using ConverterCore.Processors;
using VideoConverter.Views.ConvertDialog;
using VideoConverter.Views.FileInfo;
using VideoConverter.Views.Shell;
using VideoConverter.Views.Successful;
using Action = System.Action;

namespace VideoConverter.CoRutines
{
    public class ConverterRoutine : IResult
    {
        private readonly string outputFolder;
        private readonly IMediaConverter converter;

        private readonly List<InputVideoViewModel> source;



        public ConverterRoutine(string outputFolder, IMediaConverter converter)
        {
            this.outputFolder = outputFolder;
            this.converter = converter;
            source = new List<InputVideoViewModel>();
        }

        public void AddSource(InputVideoViewModel model)
        {
            this.source.Add(model);
        }


        public void Execute(ActionExecutionContext context)
        {

            Dispatcher currentDispatcher = Dispatcher.CurrentDispatcher;



            Task.Factory.StartNew(() =>
                {
                    Convert(currentDispatcher);
                    Completed(this, new ResultCompletionEventArgs());
                });

        }

        private void Convert(Dispatcher dispatcher)
        {
            AutoResetEvent evt = new AutoResetEvent(false); //Convert one at a time
            EncodeFinishedEventArgs result = null;

            foreach (var media in source)
            {

                media.Progress = 0;
                SingleVideoEncodingService service = new SingleVideoEncodingService
                    {
                        OutputFolder = this.outputFolder,
                        Converter = this.converter
                    };

                InputVideoViewModel closureMedia = media;
                service.Progress += (a, b) =>
                    {
                        closureMedia.Progress = b.Percentage;
                    };

                service.Done += (a, b) =>
                {
                    result = b;
                    if (b.IsCancelled)
                        closureMedia.Status = VideoConversionState.Cancelled;

                    closureMedia.Status = result.Success ?
                       VideoConversionState.Successful :
                       VideoConversionState.Errored;

                    if (closureMedia.Status == VideoConversionState.Successful)
                    {
                        Action x = null;
                        Caliburn.Micro.Execute.OnUIThreadAsync(() =>
                            {
                                DisplayResultWindow((EncoderBase)a);
                            });
                        
                    }
                    evt.Set();



                };
                media.Status = VideoConversionState.Pending;
                service.BeginEncodeVideo(media.MediaInfo);
                evt.WaitOne();

            }

        }

        private void DisplayResultWindow(EncoderBase media)
        {
            
            string folder = media.OutputFile; // it will be directory for DVD
            var manager = IoC.Get<IWindowManager>();
            //Window settings=new Window();
            IDictionary<string,object> settings=new Dictionary<string, object>();
            settings["WindowStartupLocation"]=WindowStartupLocation.CenterOwner;
            var model = new ResultViewModel();
            model.OutputFolder = folder;
            manager.ShowDialog(model,settings:settings);
        }

        public event EventHandler<ResultCompletionEventArgs> Completed = delegate { };

    }

    public static class TaskExtension
    {
        public static Task Run(this Action action)
        {
            return Task.Factory.StartNew(action);
        }
    }
}