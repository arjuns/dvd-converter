using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using VideoConverter.Views.FileInfo;

namespace VideoConverter.Converters
{
    public class StatusToProgressBarVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            VideoConversionState status = (VideoConversionState)value;
            if (status == VideoConversionState.Pending)
                return Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}