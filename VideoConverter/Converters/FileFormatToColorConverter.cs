using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace VideoConverter.Converters
{
    public class FileFormatToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch (value.ToString())
            {
                case ".AVI":
                    return new SolidColorBrush((Color)ColorConverter.ConvertFromString("green"));
                case ".MP4":
                    return new SolidColorBrush((Color)ColorConverter.ConvertFromString("lightgreen"));
                case ".MKV":
                    return new SolidColorBrush((Color)ColorConverter.ConvertFromString("orange"));

            }

            return new SolidColorBrush((Color)ColorConverter.ConvertFromString("#252525"));

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}