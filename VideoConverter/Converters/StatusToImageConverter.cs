using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using VideoConverter.Views.FileInfo;

namespace VideoConverter.Converters
{
    public class StatusToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            VideoConversionState status = (VideoConversionState)value;
            if (status == VideoConversionState.Successful)
                return "\uF14A";
            if (status == VideoConversionState.Cancelled)
                return "\uF05C";
            if (status == VideoConversionState.Errored)
                return "\uF057";
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class StatusToBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            VideoConversionState status = (VideoConversionState)value;
            if (status == VideoConversionState.Successful)
                return Brushes.Green;
            if (status == VideoConversionState.Cancelled)
                return Brushes.DarkOrange;
            if (status == VideoConversionState.Errored)
                return Brushes.Red;
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}