﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caliburn.Micro;
using TinyIoC;
using VideoConverter.Views.Shell;
using VideoConverter.Services;

namespace VideoConverter
{
    public class AppBootstrapper : Bootstrapper<IShellViewModel>
    {
        private IContainer simpleContainer;


        protected override void Configure()
        {

            this.simpleContainer = new TinyIocContainerAdaptor();
            this.simpleContainer.RegisterPackages(typeof(AppBootstrapper).Assembly);

        }

        protected override object GetInstance(Type serviceType, string key)
        {

            return this.simpleContainer.GetInstance(serviceType);
        }





        protected override IEnumerable<object> GetAllInstances(Type serviceType)
        {
            return this.simpleContainer.GetAllInstances(serviceType);
        }

        protected override void BuildUp(object instance)
        {
            this.simpleContainer.Verify();
        }
    }


    public class TinyIocContainerAdaptor:IContainer
    {
        private readonly TinyIoCContainer current = TinyIoCContainer.Current;

        public void RegisterSingle<RegisterType, RegisterImplementation>() where RegisterType: class 
            where RegisterImplementation:class ,RegisterType

        {
            
            current.Register<RegisterType, RegisterImplementation>().AsSingleton();
        }

        public void Register<RegisterType, RegisterImplementation>() where RegisterType : class where RegisterImplementation : class, RegisterType
        {
            current.Register<RegisterType, RegisterImplementation>();
        }

        public TSource GetInstance<TSource>() where TSource:class 
        {
            return current.Resolve<TSource>();
            
        }

        public object GetInstance(Type type)
        {
            return current.Resolve(type);
        }

        public object[] GetAllInstances(Type type)
        {
            return current.ResolveAll(type).ToArray();
        }

        public void Verify()
        {
            
        }
    }
    
    
}