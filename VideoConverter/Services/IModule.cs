﻿
namespace VideoConverter.Services
{
    public interface IModule
    {
        void RegisterServices(IContainer container);

    }
}