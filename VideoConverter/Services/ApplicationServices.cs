﻿using VideoConverter.Views.Shell;

namespace VideoConverter.Services
{
    public class ApplicationServices:IModule
    {
        public void RegisterServices(IContainer container)
        {
            container.RegisterSingle<IShellViewModel, ShellViewModel>();
        }
    }
}