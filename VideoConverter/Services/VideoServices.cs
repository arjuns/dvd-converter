﻿using VideoConverter.Store;

namespace VideoConverter.Services
{
    public class VideoServices : IModule
    {
        public void RegisterServices(IContainer container)
        {
          //  container.Register<IVideoEncodingService, VideoEncodingService>();
            container.Register<IThumbnailStore, ThumbnailStore>();
        }
    }
    
}