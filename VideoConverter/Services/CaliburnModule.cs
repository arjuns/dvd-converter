﻿using Caliburn.Micro;

namespace VideoConverter.Services
{
    public class CaliburnModule : IModule
    {
        public void RegisterServices(IContainer container)
        {
            container.RegisterSingle<IWindowManager, MyWindowManager>(); //singletone
            container.RegisterSingle<IEventAggregator, EventAggregator>(); //singletone
        }
    }
}
