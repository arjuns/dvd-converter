﻿using System.Collections.Generic;
using VideoConverter.Skin;
using VideoConverter.Views.ConvertDialog;

namespace VideoConverter.Features.DVD
{
    public class MP4ToDvdApplication : FeatureSelector
    {
        public override string ApplicationName
        {
            get { return "Free MP4 to DVD Converter"; }
        }
        public override string FileFilter
        {
            get { return "MP4 Files | *.mp4;"; }
        }

        public override string Version
        {
            get { return "2.1.0"; }
        }
        public override ApplicationSkin DefaultSkin
        {
            get
            {
                return ApplicationSkin.Red;
            }
        }

        public override Dictionary<VideoFormat, string> CreateFormatMap()
        {
            var formatMap = new Dictionary<VideoFormat, string>();
            formatMap.Add(VideoFormat.Mp4, ".mp4");
            return formatMap;
        }
    }
}