﻿using System.Collections.Generic;
using VideoConverter.Skin;
using VideoConverter.Views.ConvertDialog;

namespace VideoConverter.Features.DVD
{
    public class AVIToDvdApplication : FeatureSelector
    {
        public override string Version
        {
            get { return "1.0.0"; }
        }
        public override string ApplicationName
        {
            get { return "Free AVI to DVD Converter"; }
        }

        public override string FileFilter
        {
            get { return "AVI Files |*.avi"; }
        }
        public override ApplicationSkin DefaultSkin
        {
            get
            {
                return ApplicationSkin.Green;
            }
        }

        public override Dictionary<VideoFormat, string> CreateFormatMap()
        {
            var formatMap = new Dictionary<VideoFormat, string>();
            formatMap.Add(VideoFormat.Avi, ".avi");
            return formatMap;
        }
    }
}