﻿using System.Collections.Generic;
using VideoConverter.Skin;
using VideoConverter.Views.ConvertDialog;

namespace VideoConverter.Features.DVD
{
    public class MkvToDvdApplication:FeatureSelector
    {
        public override string ApplicationName
        {
            get { return "Free MKV to DVD Converter"; }
        }
        public override string Version
        {
            get { return "2.1.0"; }
        }
        public override string FileFilter
        {
            get { return "MKV Files |*.mkv"; }
        }
        public override ApplicationSkin DefaultSkin
        {
            get
            {
                return ApplicationSkin.Purple;
            }
        }
        public override Dictionary<VideoFormat, string> CreateFormatMap()
        {
            var formatMap = new Dictionary<VideoFormat, string>();
            formatMap.Add(VideoFormat.Mkv, ".mkv");
            return formatMap;
        }
    }
}