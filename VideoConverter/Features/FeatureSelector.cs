﻿using System.Collections.Generic;
using System.Linq;
using VideoConverter.Features.DVD;
using VideoConverter.Views.ConvertDialog;

namespace VideoConverter.Features
{
    public abstract class FeatureSelector
    {
        public static string CompanyName = @"FreedomSoftwareCompany";
        public abstract string ApplicationName { get; }
        public abstract string Version { get; }

        public string[] GetAvailableFileFormat()
        {
            return CreateFormatMap().Values.ToArray();
        }
        public VideoFormat[] GetValidVideoFormat()
        {
            return CreateFormatMap().Keys.ToArray();
        }
        public  string GetExtensionFor(VideoFormat format)
        {
            return CreateFormatMap()[format];
        }
        public virtual Skin.ApplicationSkin DefaultSkin { get; private  set; }
        public virtual string FileFilter
        {
            get
            {
                return "";
            }
        }

        public abstract Dictionary<VideoFormat, string> CreateFormatMap();

        public static FeatureSelector Current
        {
            get
            {

               // return new MP4ToDvdApplication();
                return new MkvToDvdApplication();
               // return new AVIToDvdApplication();
                
            }
        }

        


    }
}