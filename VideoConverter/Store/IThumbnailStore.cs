﻿namespace VideoConverter.Store
{
    public interface IThumbnailStore
    {
        string GetUniqueThubnailImagePath();
        void EnsureTempDirectory();
        void Clean();
    }
}