using ConverterCore.Encoders;
using ConverterCore.FFMpeg;

namespace ConverterCore.Converters
{
    public interface IMediaConverter
    {
        string Name { get; }
        string Description { get; }
        string Extension { get; }
        EncoderBase CreateEncoder(MediaInformation info, string outputFile);
    }
}