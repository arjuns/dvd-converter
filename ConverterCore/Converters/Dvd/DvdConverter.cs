using ConverterCore.Encoders;
using ConverterCore.FFMpeg;

namespace ConverterCore.Converters.Dvd
{
    public class DvdConverter : IMediaConverter
    {
        private readonly string argument;
        public string Name { get; set; }
        public string Description { get; set; }
        public string Extension
        {
            get { return ".mpeg"; }
        }

        public DvdConverter(string argument)
        {
            this.argument = argument;
        }

        public EncoderBase CreateEncoder(MediaInformation info, string outputFile)
        {
            return new DvdEncoder(argument, info, outputFile);
        }
    }
}