using System.Collections;
using System.Collections.Generic;

namespace ConverterCore.Converters.Dvd
{
    public class DvdConverters : IEnumerable<IMediaConverter>
    {
        readonly List<IMediaConverter> converters = new List<IMediaConverter>();

        public DvdConverters()
        {
            AddDefault();
        }

        
        private void AddDefault()
        {
            DvdConverter one = new DvdConverter("-f dvd -vcodec mpeg2video -r 29.97 -vf scale=352:480 -aspect 4:3 -b:v 4000k -mbd rd -trellis 1 -flags +mv0 -cmp 2 -subcmp 2 -acodec mp2 -b:a 192k -ar 48000 -ac 2");
            one.Name = "NTSC DVD Fullscreen";
            one.Description = "Converts to NTSC DVD Full Screen";

            DvdConverter two = new DvdConverter("-f dvd -vcodec mpeg2video -r 29.97 -vf scale=352:480 -aspect 16:9 -b:v 4000k -mbd rd -trellis 1 -flags +mv0 -cmp 2 -subcmp 2 -acodec mp2 -b:a 192k -ar 48000 -ac 2");
            two.Name = "NTSC DVD Widescreen";
            two.Description = "Converts to NTSC DVD Wide Screen";

            DvdConverter three = new DvdConverter("-f dvd -target ntsc-dvd -r 29.97 -vf scale=720:480 -aspect 4:3 -b:v 8000k -mbd rd -trellis 1 -flags +mv0 -cmp 0 -subcmp 2");
            three.Name = "NTSC DVD HQ Fullscreen";
            three.Description = "Converts NTSC DVD High Quality Fullscreen";

            DvdConverter four = new DvdConverter("-f dvd -target ntsc-dvd -r 29.97 -vf scale=720:480 -aspect 16:9 -b:v 8000k -g 12 -mbd rd -trellis 1 -flags +mv0 -cmp 0 -subcmp 2");
            four.Name = "NTSC DVD HQ Widescreen";
            four.Description = "Converts NTSC DVD High Quality Widescreen";

            DvdConverter five = new DvdConverter("-f dvd -target ntsc-dvd -b:v 5000k -r 29.97 -vf scale=720:480 -ar 48000 -b:a 384k");
            five.Name = "NTSC DVD Fast (LQ)";
            four.Description = "Converts NTSC DVD Low Quality Fast";

            DvdConverter six = new DvdConverter("-f dvd -target ntsc-dvd -b:v 5000k -r 29.97 -vf scale=720:480 -ar 48000 -b:a 384k");
            six.Name = "NTSC DVD (HQ)";
            six.Description = "Converts NTSC DVD High Quality ";
            
            converters.AddRange(new[]{one,two,three,four,five,six});
        }

        public IEnumerator<IMediaConverter> GetEnumerator()
        {
            return converters.GetEnumerator();
        }
        public void Add(IMediaConverter converter)
        {
            converters.Add(converter);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}