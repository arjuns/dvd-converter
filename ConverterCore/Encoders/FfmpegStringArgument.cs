namespace ConverterCore.Encoders
{
    public class FfmpegStringArgument:IFfmpegArgument
    {
        private readonly string args;

        public FfmpegStringArgument(string args)
        {
            this.args = args;
        }

        public string Build()
        {
            return args;
        }
    }
}