using System.Diagnostics;
using System.IO;
using ConverterCore.FFMpeg;
using ConverterCore.Processors;

namespace ConverterCore.Encoders
{
    public class DvdEncoder : MPEGEncoder
    {
        public DvdEncoder(string argument, MediaInformation mediaInfo, string outputFile)
            : base(argument, mediaInfo, outputFile)
        {
        }
        protected override void DoEncodeFinished(EncodeFinishedEventArgs e)
        {
            Process author = GetDvdAuthor();
            using (author)
            {
                try
                {
                    author.Start();
                    author.WaitForExit();
                    File.Delete(OutputFile);
                    OutputFile = GetDvdDirectory(); // HACK


                }
                finally
                { 
                   
                    base.DoEncodeFinished(e);
                   
                }
            }
            
        }
        protected Process GetDvdAuthor()
        {
            var newDir = GetDvdDirectory();

         
            if (Directory.Exists(newDir))
                Directory.Delete(newDir, true);

            Directory.CreateDirectory(newDir);
            string args = string.Format("-o {0} -t {1}", newDir.QuotedString(), OutputFile.QuotedString());
            Process process = new Process();
            ProcessStartInfo info = new ProcessStartInfo();
            info.FileName = "DvdAuthor.exe";
            info.Arguments = args;
            info.CreateNoWindow = true;
            info.UseShellExecute = false;
            process.StartInfo = info;
            return process;
        }

        private string GetDvdDirectory()
        {
            string rootDir = Path.GetDirectoryName(OutputFile);
            string filename = Path.GetFileNameWithoutExtension(OutputFile);
            string newDir = Path.Combine(rootDir, filename);
            return newDir;
        }
    }
}