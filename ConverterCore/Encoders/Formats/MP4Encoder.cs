using System;
using System.Collections.Generic;
using ConverterCore.FFMpeg;
using ConverterCore.Processors;

namespace ConverterCore.Encoders
{
    public class MP4Encoder : EncoderBase
    {
        public MP4Encoder(MediaInformation mediaInfo,
                            string outputFile)
            : base(mediaInfo, outputFile)
        {

        }
        protected override IFfmpegArgument CreateParameter()
        {
            var arg = (FfmpegArgument)base.CreateParameter();
            arg.AddOption("f", String.Format("avi {0}", this.OutputFile.QuotedString()));
            return arg;
        }
    }
    
}