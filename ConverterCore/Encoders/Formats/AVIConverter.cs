using System;
using ConverterCore.FFMpeg;
using ConverterCore.Processors;

namespace ConverterCore.Encoders
{
    public class AVIEncoder : EncoderBase
    {
        public AVIEncoder(MediaInformation mediaInfo,
                            string outputFile)
            : base(mediaInfo, outputFile)
        {

        }
        protected override IFfmpegArgument CreateParameter()
        {
            var arg = (FfmpegArgument)base.CreateParameter();
            arg.AddOption("f", String.Format("avi {0}", this.OutputFile.QuotedString()));
            return arg;
        }


    }
}