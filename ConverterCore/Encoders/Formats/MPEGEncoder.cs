using System.Text;
using ConverterCore.FFMpeg;
using ConverterCore.Processors;

namespace ConverterCore.Encoders
{
    public class MPEGEncoder : EncoderBase
    {
        private readonly string argument;

        public MPEGEncoder(string argument, MediaInformation mediaInfo, string outputFile)
            : base(mediaInfo, outputFile)
        {
            this.argument = argument;
        }



        protected override IFfmpegArgument CreateParameter()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("-y -i {0} ", MediaInfo.OriginalFilePath.QuotedString());
            sb.Append(argument);
            sb.Append(" " + OutputFile.QuotedString());
            return new FfmpegStringArgument(sb.ToString());
        }

    }
}