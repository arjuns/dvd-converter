using System;
using ConverterCore.FFMpeg;
using ConverterCore.Processors;

namespace ConverterCore.Encoders
{
    public class MkvEncoder : EncoderBase
    {
        public MkvEncoder(MediaInformation mediaInfo,
            string outputFile)
            : base(mediaInfo, outputFile)
        {

        }
        protected override IFfmpegArgument CreateParameter()
        {
            //Matroska file format (mkv) is specified with -f matroska option.
            //It should be supported by ffmpeg (version 0.7.3) in Ubuntu 11.10. Use ffmpeg -formats for a list of supported file formats.

            var arg = (FfmpegArgument)base.CreateParameter();
            arg.AddOption("f", String.Format("matroska {0}", this.OutputFile.QuotedString()));
            return arg;
        }
    }
}