using System.Collections.Generic;
using System.Text;

namespace ConverterCore.Encoders
{
    public class FfmpegArgument:IFfmpegArgument
    {
        private readonly Dictionary<string, string> arguments = new Dictionary<string, string>();
        public void AddOption(string key, string value = "")
        {
            arguments.Add(key, value??"");
        }
        
        public string Build()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var pair in arguments)
            {
                string opt = pair.Key.Trim();
                string val = pair.Value.Trim();
                if (!opt.StartsWith("-"))
                    opt = "-" + opt;
                sb.AppendFormat("{0} {1} ", opt, val);
            }
            return sb.ToString().Trim();
        }
        public override string ToString()
        {
            return Build();
        }


    }

    public interface IFfmpegArgument
    {
        string Build();
    }
}