using System;

namespace ConverterCore.Encoders
{
    public class ProcessExitEventArgs:EventArgs
    {
        public int ExitCode { get; private set; }
        public ProcessExitEventArgs(int exitCode)
        {
            this.ExitCode = exitCode;
        }
    }
}