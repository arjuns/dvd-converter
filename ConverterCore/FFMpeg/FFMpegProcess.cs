using System;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using ConverterCore.Encoders;

namespace ConverterCore.FFMpeg
{
    public class FFMpegProcess
    {
        public bool IsRunning { get; private set; }
        private readonly Process innerProcess;
        public event EventHandler<ProcessLogEventArgs> ErrorData = delegate { };
        public event EventHandler<ProcessExitEventArgs> ProcessFinished = delegate { };


        public FFMpegProcess(string arguments)
        {

            innerProcess = new Process();
            innerProcess.EnableRaisingEvents = true;
            
            
            innerProcess.ErrorDataReceived += ErrorDataReceived;
            innerProcess.Exited += ProcessExited;
            var startInfo = new ProcessStartInfo("ffmpeg.exe", arguments)
                                {
                                    UseShellExecute = false,
                                    CreateNoWindow = true,
                                    RedirectStandardError = true
                                };
            innerProcess.StartInfo = startInfo;
        }

       

        private void ProcessExited(object sender, EventArgs e)
        {
            IsRunning = false;
            ProcessFinished(this, new ProcessExitEventArgs(this.innerProcess.ExitCode));
        }

        private void ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            ErrorData(this, new ProcessLogEventArgs(e.Data));
        }

        public virtual string Start()
        {
            if (IsRunning)
                throw new Exception("Process Already Running");
            innerProcess.Start();
            this.IsRunning = true;
            innerProcess.WaitForExit();
            return innerProcess.StandardError.ReadToEnd();
        }
        public virtual void StartAsync()
        {
            if (IsRunning)
                throw new Exception("Process Already Running");
            innerProcess.Start();
            this.IsRunning = true;
            string line = "";
            while ((line = innerProcess.StandardError.ReadLine()) != null)
            {
                ErrorData(this,new ProcessLogEventArgs(line));
            }
            innerProcess.WaitForExit();
            
        }


        public static Process[] GetAllRunningFFMpegProcess()
        {
            return Process.GetProcessesByName("ffmpeg");
        }

        public void SetArgument(string parameters)
        {
            this.innerProcess.StartInfo.Arguments = parameters;
        }

        public void Kill()
        {
            if (!innerProcess.HasExited)
            {
                try
                {
                    Process process = Process.GetProcessById(innerProcess.Id);
                    process.Kill();
                }
                catch (Exception ex)
                {

                }
            }
        }

        private bool infoExtracted = false;
        protected TimeSpan ExtractDuration(string rawInfo)
        {
            TimeSpan t = TimeSpan.Zero;
            Regex re = new Regex("[D|d]uration:.((\\d|:|\\.)*)", RegexOptions.Compiled);
            Match m = re.Match(rawInfo);

            if (m.Success)
            {
                string duration = m.Groups[1].Value;
                string[] timepieces = duration.Split(new char[] { ':', '.' });
                if (timepieces.Length == 4)
                {
                    t = new TimeSpan(0, Convert.ToInt16(timepieces[0]), Convert.ToInt16(timepieces[1]), Convert.ToInt16(timepieces[2]), Convert.ToInt16(timepieces[3]));
                }
            }

            return t;
        }
    }
}