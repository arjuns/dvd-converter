using System;

namespace ConverterCore.FFMpeg
{
    public class EncodeProgressEventArgs : EventArgs
    {
        public string RawOutputLine { get; set; }
        public short FPS { get; set; }
        public double Percentage { get; set; }
        public long CurrentFrame { get; set; }
        public long TotalFrames { get; set; }
        public string OriginalFileName { get; set; }
    }
}