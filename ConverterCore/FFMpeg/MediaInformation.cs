﻿using System;

namespace ConverterCore.FFMpeg
{

    public class MediaInformation
    {
        
        public string OriginalFilePath { get; set; }
        public TimeSpan Duration { get; set; }
        public double BitRate { get; set; }
        public string RawAudioFormat { get; set; }
        public string AudioFormat { get; set; }
        public string RawVideoFormat { get; set; }
        public string VideoFormat { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public string RawInfo { get; set; }
        public double FrameRate { get; set; }
        public long TotalFrames { get; set; }
        public double AudioBitRate { get; set; }
        public double VideoBitRate { get; set; }

        

        public MediaInformation(string path)
        {
            OriginalFilePath = path;
        }
        
    }
}