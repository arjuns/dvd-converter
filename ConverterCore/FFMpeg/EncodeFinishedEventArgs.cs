using System;

namespace ConverterCore.FFMpeg
{
    public class EncodeFinishedEventArgs : EventArgs
    {
        public bool IsCancelled { get; set; }
        public bool Success { get; set; }
        
    }
}